#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define N 100 // size of the name of a program  

int main(int argc, char **argv) {
    int sec = 0;
    FILE *f = fopen(argv[argc-1], "r");
    printf("%s\n", argv[argc-1]);
    char *name_exec = calloc(sizeof(char), N);                  // allocate N cells for char vars into name_exec string
    while(fscanf(f, "%d %[^\n]s", &sec, name_exec) == 2) {      // read strings line by line with N as max size until 
        sleep(sec);                                             // the EOF reached 
        system(name_exec);
        sec = 0;
        
    }
    fclose(f);
    free(name_exec);
    return 0;
}