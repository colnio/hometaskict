#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <malloc.h>

#define N 1000                 // N is the max size of a command


int unzip(char *path) {       // Function gets path of zipped
  DIR *dir;                   // directory and uncompress it
  dir = opendir(path);        // with gzip built-in program
  if (dir == NULL) {
    return(-1);
  }
  pid_t pid;
  pid = fork();
  if (pid == -1) {
      printf("Can't create child.\n");
      exit(-1);
  } else if (pid == 0) {
  execlp("gzip", "gzip", "-dNr", path, NULL);
  }
  while (wait(NULL) != -1) {}
  return(0);
}

int zip(char *path) {          // Function gets path of zipping directory
  pid_t pid;                   // and compress it calling gzip
  pid = fork();                // linux built-in program
  if (pid == -1) {
    printf("Fork error.\n");
    exit(-1);
  } else if (pid == 0) {
  execlp("gzip", "gzip", "-Nr", path, NULL);
  }
  while (wait(NULL) != -1) {}
  return(0);
}

int backup(char *inpath, char *outpath) {
  pid_t pid;
  DIR *sourcedir = opendir(inpath);
  DIR *destdir = opendir(outpath);
  struct dirent *readingtmp;
  char *str[N];
  char buf[N];
  int i = 2, k;
  str[0] = "cp";
  str[1] = "-upr";

  if (sourcedir == NULL) {
    printf("Can't open source directory.\n");
    exit(-1);
  }

  if (destdir == NULL) {
    mkdir(outpath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  }
  if (sourcedir == destdir) {
    printf("You can't backup directory into itself.\n");
    exit(-1);
  }
  if ((readingtmp = readdir(sourcedir)) == NULL) {
    printf("Source directory is empty.\n");
    exit(-1);
  }
  rewinddir(sourcedir);

  while (readingtmp = readdir(sourcedir)) {
    if(!strcmp(readingtmp->d_name, ".") || !strcmp(readingtmp->d_name, ".."))
        continue;
    memset(buf, 0, sizeof(buf));
    strcpy(buf, inpath);
    strcat(buf, "/");
    strcat(buf, readingtmp->d_name);
    str[i] = malloc(strlen(buf)+1);
    strcpy(str[i], buf);
    i++;
  }

  str[i] = outpath;
  closedir(sourcedir);
  closedir(destdir);
  pid = fork();
  if (pid == -1) {
    printf("Fork error.\n");
    exit(-1);
  } else if (pid == 0) {
    execvp(str[0], str);
  }
  while (wait(NULL) != -1) {}
  for (k = 2; k < i; k++) {
    free(str[k]);
  }
  return 0;
}


int main(int argc, char *argv[]){
  if(argc != 3){
      printf("Incorrect arguments format. Exiting.\n");
      exit(-1);
  }
  unzip(argv[2]);
  backup(argv[1], argv[2]);
  zip(argv[2]);
  return 0;
}
